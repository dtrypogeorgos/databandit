from setuptools import setup
from pathlib import Path

# ------------------ make .rc file
rc_template = ['# databandit configuration file\n\n',
               '[ftp]\n',
               'server = <server-name>\n',
               'username = <username>\n',
               'password = <password>\n',
               '<ports> = <comma-separated-port-list>\n']

p = Path.home() / '.databandit' / 'databanditrc'
if not p.exists():
    p.parent.mkdir(exist_ok=True)
    with p.open('w') as f:
        f.writelines(rc_template)


with open('Readme.md') as readme_file:
    readme = readme_file.read()

setup(
    name='databandit',
    version='0.3.3',
    description="Software aide for analysis of experimental data.",
    long_description=readme,
    author="Dimitris Trypogeorgos",
    author_email='dtrypo@umd.edu',
    url='https://bitbucket.org/dtrypogeorgos/databandit',
    packages=['databandit'],
    include_package_data=True,
    install_requires=['h5py>=2.6.0',
                      'pandas>=0.19.2',
                      'tqdm>=4.11.2',
                      'tables>=3.3.0',
                      'Click>=6.7.0'],
    entry_points='''
        [console_scripts]
        databandit=databandit.cli:cli
    ''',
    license="MIT license",
    zip_safe=False,
    keywords='databandit',
    classifiers=[
        'Intended Audience :: Scientists',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ]
)
