from databandit import checkfailed
import h5py

# print(data)

with h5py.File(data, 'r') as f:
    attrs = f['results/rois_od'].attrs

    g_p0 = attrs['roi_0']
    g_p1 = attrs['roi_1']
    g_p2 = attrs['roi_2']

    attrs = f['globals'].attrs
    g_arpfinebias = attrs['ARP_FineBias']
    g_delta_xyz = attrs['delta_xyz']
