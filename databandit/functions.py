from ftplib import FTP
from pathlib import Path
import time
from tqdm import tqdm
import pandas as pd
import h5py
import shutil
from collections import OrderedDict
import logging

log = logging.getLogger(__name__)


def getftpfiles(servername, port, username, password, files, localpath,
                onlydiff=True, passive=False):

    requestedfiles = len(files)
    filesmissing = requestedfiles

    # TODO need to short this out

    if onlydiff:
        def listrelativepath():
            for localfile in Path(localpath).iterdir():
                yield Path(localfile).relative_to(Path('.'))

        localfiles = set(map(str, listrelativepath()))
        files = set(files) - localfiles

        filesmissing = len(files)
        if filesmissing == 0:
            print('All files in local folder.')
            return

    print('Connecting to FTP {}:{}'.format(servername, port))
    ftp = FTP()
    ftp.connect(servername, port, timeout=10)
    ftp.login(username, password)
    ftp.set_pasv(passive)

    rfiles = set(ftp.nlst(str(localpath)))
    files = rfiles & set(files)  # get the intersection

    print(f'Requested: {requestedfiles}')
    print(f'Not on local: {filesmissing}')
    print(f'Not on server: {filesmissing - len(files)}')

    time.sleep(1)  # give time to tqdm to load

    for rfile in tqdm(files, desc='Transfer', leave=True):
        with open(rfile, 'wb') as f:
            ftp.retrbinary('RETR ' + rfile, f.write)

    time.sleep(1)  # just to be on the safe side

    print('Connection closed.')
    ftp.close()  # close FTP connection
    log.info('Got files from ftp.')


def savedataframe(filename, dataframe):

    dataframe.to_hdf(filename, 'dataframe', append=True)


def loaddataframe(filename):
    try:
        return pd.read_hdf(filename, 'dataframe')
        log.info('Loading dataframe from file.')
    except KeyError:
        return pd.DataFrame()


def _loadgroup(filename, group, idx):

    def to_dict(name, obj):
        data[name] = obj.value

    with h5py.File(filename, 'r') as f:

        group = f[f'/{group}/{idx:03}']
        data = {}

        group.visititems(to_dict)
        data.update(group.attrs)

    return data


def loaddata(filename, group, idx=None):

    if idx is not None:
        return _loadgroup(filename, group, idx)

    with h5py.File(filename, 'r') as f:
        keys = f[group].keys()
        data = dict.fromkeys(keys, {})

        for key in keys:
            data[key] = _loadgroup(filename, group, int(key))

    return OrderedDict(data)


def extractscript(filename, script):

    p = Path(filename)
    ps = Path(script)
    outfile = f'{p.stem}_{ps.name}'

    with h5py.File(filename, 'r') as f:
        dset = f[f'scripts/{script}']

        with open(outfile, 'w') as f:
            f.write(dset.value)

    return outfile


def resultsonly(filename):

    p = Path(filename)
    newname = p.parent / ('resultsonly_' + p.name)

    shutil.copyfile(filename, newname)

    with h5py.File(str(newname), 'r+') as f:
        # TODO check that this indeec reduces the size of the file and it
        # doesn't just leave the data folder dangling
        del f['data']
        f['data'] = h5py.ExternalLink(filename, '/data')
