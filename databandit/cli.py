import click
import h5py
from pathlib import Path
import shutil
from . import functions
import subprocess


def _delete(item, folder=False):
    try:
        if not folder:
            Path(item).unlink()
        else:
            shutil.rmtree(item)

        click.echo(f'Deleted {item}.')
    except FileNotFoundError:
        click.echo(f'{item} not found.')


@click.group()
def cli():
    """cli interface for databandit.
    """
    pass


@cli.command()
@click.confirmation_option(help='Deletes without confirmation.')
@click.option('--log', is_flag=True, help='Deletes the databandit.log')
@click.option('--data', type=click.Path(), help='Deletes the data folder.')
@click.option('--results', is_flag=True, help='Deletes the results folder.')
def delete(log, data, results):
    """Deletes databandit generated folders/files.
    """

    if log:
        _delete('databandit.log')

    if data:
        _delete(data, folder=True)

    if results:
        _delete('results', folder=True)


@cli.command()
@click.argument('filename')
def process(filename):
    """Reruns postprocess from databandit h5 file.
    """

    def get_script(name, obj):
        if 'postprocess' in obj.attrs.get('tag', ()):
            return name, obj

    with h5py.File(filename, 'r') as f:
        script, dset = f['scripts'].visititems(get_script)
        sandbox = {}
        sandbox.update(dset.attrs)

    outfile = functions.extractscript(filename, script)

    click.echo(f'Rerunning postprocess from {filename}')
    exec(open(outfile).read(), sandbox, sandbox)

    # delete the file after it runs
    Path(outfile).unlink()


@cli.command()
@click.option('--edit', is_flag=True, help='Edit configuration file with vim.')
def config(edit):
    """See databandit configuration options.
    """

    config_file = Path.home() / '.databandit/databanditrc'

    if edit:
        subprocess.run(['vi', config_file])
        return

    with open(config_file, 'r') as f:
        click.echo(f.read())


@cli.command()
def combine(files):
    """Combine files to a single h5 file.
    """
    # TODO combine sequences gracefully for broken up data
    pass
