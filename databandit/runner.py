import importlib.util
from pathlib import Path
from . import utils
from . import functions
import inspect
from . import gobbler as gob
import configparser
import h5py
import pandas as pd
from tqdm import tqdm
import logging
import copy
import contextlib
import sys

__version__ = '0.3.3'

log = logging.getLogger(__name__)
# TODO put more logging statements


def logger():
    # simple logging
    logfile = Path.cwd() / 'databandit.log'
    logging.basicConfig(
        filename=logfile, filemode='w', level=logging.INFO,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def checkfailed(message='checkfailed'):
    raise DataSequenceError(message)


class DataSequenceError(Exception):
    pass


class DataSequence():

    def __init__(self, cfg_file=None, *args, **kwargs):
        logger()  # initalise the logger

        if cfg_file is not None:
            self.config = self._config(cfg_file, **kwargs)

    def __add__(self, other):
        if other.config != self.config:
            raise DataSequenceError('Cannot combine DataSequences ',
                                    'configured with different config files.')

        cfg1 = self.config
        cfg2 = other.config
        cfg = copy.copy(self.config)  # copy one or the other

        def filenames():
            return cfg1.filenames() + cfg2.filenames()

        def outputfile():
            return Path(cfg1.outputfile()).stem + '_' + cfg2.outputfile()

        ds = DataSequence()
        cfg.filenames = filenames
        cfg.outputfile = outputfile
        ds.config = cfg

        return ds

    def __radd__(self, other):
        if other == 0:
            return self
        return self.__add__(other)

    def _cfg_filepath(self, cfg_file):
        """Looks for a configuration file that describes the data format.
        If the cfg_file filepath is absolute it will just return that.
        If it is relative it will first look in the CWD and then in the
        .databandit folder.
        """

        p = Path(cfg_file)
        yield p
        log.debug('Failed to load config from absolute path.')

        # if not absolute give local folder first
        p = Path.cwd()
        yield p / cfg_file
        log.debug('Failed to load config from local folder.')

        # then .databandit
        yield Path.home() / '.databandit' / cfg_file

    def _config(self, cfg_file, **kwargs):
        """Loads the cfg_file module from whichever path it finds it first.
        Temporarily adds the parent folder to PATH so any imports are done
        correctly. Then checks for a class with the same name as cfg_file
        (not case sensitive) and returns an object of that class.
        """

        filepaths = self._cfg_filepath(cfg_file)
        # TODO handle symbolic links
        for filepath in filepaths:
            with contextlib.suppress(FileNotFoundError):
                with utils.add_to_path(filepath.parent):
                    modulename = filepath.stem

                    # only import if not imported already
                    if modulename not in sys.modules:
                        spec = importlib.util.spec_from_file_location(
                            modulename, str(filepath))
                        module = importlib.util.module_from_spec(spec)
                        spec.loader.exec_module(module)
                        sys.modules[modulename] = module
                    else:
                        module = sys.modules[modulename]

                    clsmembers = inspect.getmembers(module, inspect.isclass)
                    for clsmember in clsmembers:
                        if clsmember[0].lower() == filepath.stem:
                            config_class = getattr(module, clsmember[0])
                            return config_class(**kwargs)

        raise DataSequenceError('Configuration file not found or not correct.')

    def _createoutputfile(self):
        p = Path('./results')
        p.mkdir(exist_ok=True)
        self.outfile = str(p / self.config.outputfile())

        attrs = {'version': __version__,
                 'config': str(self.config.__class__)}

        with contextlib.suppress(OSError):
            with h5py.File(self.outfile, 'x') as f:
                f.attrs.update(attrs)

    def _safeload(self, iterable, sandbox):
        """Safeguards against something going wrong with config.loadfile()
        because of a corrupt file or something.
        """

        for item in iterable:
            idx, filename = item
            try:
                data = self.config.loadfile(filename)
                sandbox['data'] = data
                yield item, sandbox
            except Exception as e:  # catch all exceptions
                self.dataframe.loc[idx, 'status'] = repr(e)
                log.error('Exception in loadfile()', exc_info=True)

    def _exec(self, script, gobbler, sandbox, idx):
        try:
            exec(open(script).read(), sandbox, sandbox)
        except Exception:  # catch all exceptions
            gobbler._error = sys.exc_info()
        finally:
            gobbler.gobble(sandbox, idx)

    def prepare(self, script='prepare.py', redo=False, **kwargs):
        folder = 'data'

        self._createoutputfile()
        self.dataframe = functions.loaddataframe(self.outfile)

        with h5py.File(self.outfile, 'r') as f:
            prepared = f.attrs.get('prepared')
            if prepared and redo:
                self.dataframe = pd.DataFrame()  # reinitialise the dataframe
                print('Redoing prepare stage.')
                print('Deleting dataframe.')
            elif prepared and not redo:
                print('Data already exist. Skipping preparation stage.')
                return

        filenames = self.config.filenames()
        sandbox = kwargs.copy()

        with gob.Gobbler(self.outfile, folder,
                         self.dataframe, script) as gobbler:
            for (idx, filename), sandbox in self._safeload(
                    enumerate(tqdm(filenames, leave=True, desc='Preparing')),
                    sandbox):

                self.dataframe.loc[idx, 'files'] = Path(filename).name
                self._exec(script, gobbler, sandbox, idx)

        log.info(f'Prepared data successfully up to {filename}.')
        with h5py.File(self.outfile, 'r+') as f:
            f.attrs['prepared'] = 'true'

    def analyse(self, script='analyse.py', redo=False, **kwargs):
        folder = 'results'

        with h5py.File(self.outfile, 'r') as f:
            if not f.attrs.get('prepared'):
                print('No data in h5 file.')
                return

            analysed = f.attrs.get('analysed')
            if analysed and redo:
                print('Redoing analysis stage.')
            elif analysed and not redo:
                print('Skipping analysis stage.')
                return

        good_files = self.dataframe[self.dataframe['status'] == 'good']
        sandbox = kwargs.copy()

        with gob.Gobbler(self.outfile, folder,
                         self.dataframe, script) as gobbler:
            for idx in tqdm(good_files.index, leave=True, desc='Analysing'):

                data = functions.loaddata(self.outfile, 'data', idx)
                data['dataframe'] = self.dataframe.loc[idx]
                sandbox['data'] = data

                self._exec(script, gobbler, sandbox, idx)

        log.info('Analysed data successfully.')
        with h5py.File(self.outfile, 'r+') as f:
            f.attrs['analysed'] = 'true'

    def postprocess(self, script='postprocess.py', **kwargs):

        sandbox = kwargs.copy()
        sandbox['filename'] = self.outfile
        sandbox['tag'] = 'postprocess'

        gob._savescriptsource(self.outfile, script)
        gob._savescriptattrs(self.outfile, script, sandbox)

        exec(open(script).read(), sandbox, sandbox)

    def _parsercfile(self):
        p = Path.home()
        rcfile = p / '.databandit/databanditrc'

        if not rcfile.exists():
            raise DataSequenceError('databanditrc does not exist.')

        parser = configparser.SafeConfigParser()
        parser.read(str(rcfile))

        return parser

    def filesfromftp(self, onlydiff=True):

        parser = self._parsercfile()

        try:
            username = parser.get('ftp', 'username')
            password = parser.get('ftp', 'password')
            server = parser.get('ftp', 'server')
            ports = parser.get('ftp', 'ports').split(',')
        except configparser.NoSectionError:
            raise DataSequenceError('No [ftp] section in databanditrc.')

        files = self.config.filenames()

        # get the directories
        dirs = set(map(lambda _file: Path(_file).parent, files))

        for _dir in dirs:
            _dir.mkdir(exist_ok=True, parents=True)  # make local dir
            files = filter(lambda file: _dir == Path(file).parent, files)
            for port in ports:
                # TODO deal with the ports differently
                # TODO error handling?
                functions.getftpfiles(server, int(port), username, password,
                                      list(files), _dir, onlydiff)
