import h5py
import inspect
import logging

log = logging.getLogger(__name__)

# TODO put this in a multiprocessing pool. Will need to handle hdf lock access
# or use pulsar to do it async


class Gobbler():
    def __init__(self, outfile, root, dataframe, script):
        self.outfile = outfile
        self.dataframe = dataframe
        self.root = root
        self.script = script

    def gobble(self, vars, idx):
        """Saves the value of any item in the vars dict that starts with 'g_'.
        The mapping is:
        - dicts and strings become h5 attributes
        - other iterables become datasets
        - anything else (probably just single values) go to the dataframe
        Otherwise, if the _error attribute is set it will just write that to
        the dataframe end exit.
        """

        if getattr(self, '_error', False):
            self._gobble_errors(idx)
            return

        self.dataframe.loc[idx, 'status'] = 'good'
        group = f'{self.root}/{idx:03}'

        with h5py.File(self.outfile, 'r+') as f:

            for key, value in filter(lambda item: item[0].startswith('g_'),
                                     vars.items()):
                name = f'{group}/{key[2:]}'

                if type(value) in [dict, str]:
                    f[group].attrs.update(value)

                elif hasattr(value, '__iter__'):
                    self._savedataset(f, name, value)

                else:
                    self.dataframe.loc[idx, key[2:]] = value

    def _gobble_errors(self, idx):
        _, e, _ = self._error  # unpack the traceback
        msg = str(e) if str(e) else repr(e)

        self.dataframe.loc[idx, 'status'] = msg
        log.error(f'Exception in prepare idx={idx}', exc_info=self._error)
        self._error = None  # reset the error

    def _savedataset(self, filename, name, value):
        """Delete the dataset before rewriting. h5 is a pain with deleting
        stuff but this approach keeps file size from growing too much.
        """

        try:
            filename.create_dataset(name, data=value, dtype=float)
        except RuntimeError:
            del filename[name]
            filename.create_dataset(name, data=value, dtype=float)

    def __enter__(self):
        return self

    def __exit__(self, _type, value, traceback):
        self.dataframe.to_hdf(self.outfile, 'dataframe')
        print('Saving dataframe.')

        _savescriptsource(self.outfile, self.script)
        _savecallersource(self.outfile)
        print('Saving script and caller source.')

    def gurgle(self):
        # TODO should traverse attrs, datasets for idx and return a dict of all
        pass


def _savescriptattrs(filename, script, attrs):
    name = f'scripts/{script}'

    # I could json serialise them but I don't want to allow for too much
    # flexibility here. Whatever h5 can handle will do.
    with h5py.File(filename, 'r+') as f:
        try:
            f[name].attrs.update(attrs)
        except TypeError:
            response = f'Could not save {script} attrs to {filename}.'
            log.error(response)
            print(response)


def _savescriptsource(filename, script):
    name = f'scripts/{script}'

    with h5py.File(filename, 'r+') as f:
        with open(script, 'r') as pf:
            lines = pf.read()
            try:
                f.create_dataset(name, data=lines)
            except RuntimeError:
                del f[name]
                f.create_dataset(name, data=lines)


def _savecallersource(filename):
    # TODO correct this one to sys.argv
    caller = inspect.stack()[3][1]
    _savescriptsource(filename, caller)
