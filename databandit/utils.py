from contextlib import contextmanager
import sys


@contextmanager
def add_to_path(p):
    """Needed mainly because of this
    https://stackoverflow.com/questions/41861427/python-3-5-how-to-dynamically-import-a-module-given-the-full-file-path-in-the
    """
    old_path = sys.path
    sys.path = sys.path[:]
    sys.path.insert(0, p)
    try:
        yield
    finally:
        sys.path = old_path


def pkg_info():
    """ Use this to check file hierarchy in packages and avoid problems
    associated with relative imports.
    """
    print('__file__={0:<35}'.format(__file__))
    print('__name__={1:<20}'.format(__name__))
    print('__package__={2:<20}'.format(str(__package__)))
