import abc

# TODO change some of these into properties?


class Config(abc.ABC):
    # TODO documentation of methods

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def filenames(self):
        # return filenames iterable
        pass

    # TODO make this @property
    @abc.abstractmethod
    def outputfile(self):
        # return output file
        pass

    def loadfile(self, filename):
        """Returns filename by default. Override it in the subclass
        to return other data if you want.
        """
        return filename

    def __eq__(self, other):
        return isinstance(other, self.__class__)
