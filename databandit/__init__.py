from .runner import DataSequence, checkfailed, __version__
from .config import Config
from .functions import *

__author__ = 'dimitris'
__all__ = ['runner']
