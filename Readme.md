# databandit


databandit stands for `Data Batch Analysis DT` and is a scientific aide for analysis of experimental data.
It tries to be minimally intrusive and can be configured to work with any data workflow makes sense for you.

A typical experimental sequence consists of multiple files that need to be considered during the analysis stage.
databandit provides an interface that simplifies the tedious file management.
It performs a couple of sanity checks on the files and curates the data in an hdf output file.

In a typical usage pattern, the relevant data are collected into a single hdf file using a `prepare.py` script.
The `analysis.py` script then runs only on these data and the output is saved on the same hdf file.
The source of both scripts is saved along with the data so that an analysis run can be repeated only from the hdf file if necessary.
The data are saved as hdf datasets, attributes or columns of a pandas dataframe depending on their type.
You can then run a `postprocess.py` script to generate plots etc.
There is no need to use databandit for this but doing so will save the script along with the data for future reference.


# Installation

databandit is python3 only and requires a couple of packages in addition to the standard library such as `pandas`, `tqdm` and `click`.
We recommend using the Anaconda python installation which comes with batteries included and many of these packages preinstalled.
Clone this repo with

~~~
git clone https://dtrypogeorgos@bitbucket.org/dtrypogeorgos/databandit.git
~~~

`cd` to the local folder and run any of the commands below

~~~
make install
python setup.py install
pip install .
~~~

The installation script will also generate a `.databandit/.databanditrc` file in your home folder but will not overwrite any preexisting ones.


# Configuration

databandit needs to be configured by a configuration class describing the format of the filenames and directories.
This should subclass the provided class `Config` and reimplement at least `__init__, filenames(), outfile()`.
In addition you can reimplement `loadfile()` to control what the `data` variable contains, otherwise it defaults to `filename`.
Examples for [setlist](https://github.com/JQIamo/SetList) and [labscript](http://labscriptsuite.org/) are provided in the configuration folder.
databandit will look for a configuration file in the following places:

1.  If the path you initialise a `DataSequence()` object with is absolute it will just look for that and nothing more.
2.  If the path is relative, it will first look at your `pwd` and then at `$HOME/.databandit` directory.

You can also configure databandit to pull files via FPT.
For this you need a `.databanditrc` file in your `.databandit` directory written in python config file style.
It should contain at least one section like the following:

~~~
[ftp]
server = 127.0.0.1
ports = 54321, 56748
username = <your-username>
password = <your-password>
~~~

This is the only required section for FTP but future functionality might require further configuration of this file.


# Usage

You can find example files in this repository.
`dbrun.py` is the main file that makes a `DataSequence()` object and then calls `prepare.py`, `analysis.py` and `postprocess.py`.
Nothing too surprising there.

An important thing to note is the use of `data` as reserved keyword.
`data` is inserted into the global dictionary of both the prepare and analysis scripts.
By default `data` is just the filename of the file corresponding to the data being analysed, unless you have reimplemented `loadfile()` in which case it will return the output of that.
Look at the setlist configuration file for an example of this.
If an exception is raised, due to a failed check or otherwise, databandit will capture it and write it on the status column of the dataframe.


After a `prepare` or `analyse` is executed a gobbler will scan your script and save any variable you want to the hdf file.
Variables to-be-saved need to have the special g\_ prefix in their names.
`databandit` saves variables according to their type:

-   single numerical values go to a dataframe column with the same name.
-   dicts become group attributes. The actual name of the dict doesn't matter as long as it starts with g\_. Only key, value pairs are saved.
-   any other iterable becomes an hdf dataset
Use these to arrange your data in any way you want.




Have fun analysing data!


P.S. If you are looking for the old, python2, setlist-only version of _databandit_ check out commit c936b18 from master on 2016-12-29.
