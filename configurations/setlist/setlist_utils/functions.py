import os
from .qgasfileio import LoadImg
from .igorbin import NumberByKey
import numpy as np
import json


def loaddevmap():

        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                               'devicemap.json'), 'r') as fp:
            devmap = json.load(fp)

        # figure out how to subclass json decoder to do this during loading
        for k, v in devmap.items():
            devmap[k] = set(v)

        return devmap


def getdevname(filename):
    filesplit = os.path.split(filename)[1].split('_')
    pathsplit = os.path.split(filename)[0].split('/')

    start_check = [s.startswith(pathsplit[-1]) for s in filesplit].index(True)
    end_check = [s.endswith(pathsplit[-3]) for s in filesplit].index(True)

    if start_check == end_check:
        dev = '_'.join(filesplit[:start_check])
    else:
        dev = None

    return dev


def loadfile(filename, dev=None):

    if dev is None:  # guess dev from filename
        dev = getdevname(filename)

    devicemap = loaddevmap()

    if dev in devicemap['camera']:
        # return LoadIBW(filename) # temp fix till I organise igorbin
        return loadimg(filename)

    elif dev in devicemap['scope']:
        return loadscope(filename)

    elif dev in devicemap['analogin']:
        return loadai(filename)


def loadimg(filename):

    return LoadImg(filename)


def loadscope(filename, pointnum=2500):

    scope = LoadImg(filename)
    scopedata = scope['OptDepth']

    # sanity check on the size of the data
    assert np.isclose(100 * np.floor(scopedata.shape[1] / 100), pointnum)

    # trim the end of scopedata by -1?
    offset = 1
    dataslice = pointnum + offset

    pre = scopedata[:, :-dataslice]
    scopedata = scopedata[:, -dataslice:-offset]

    _rescalescopevertical(scopedata, pre)
    timerow = _rescalescopehorizontal(pre, pointnum)

    # returned data should have a Note so that isindexedwave() can find them

    scopedata = np.insert(scopedata.T, 0, timerow, axis=1)

    return {'data': scopedata,
            'Note': scope['Note']}


def _rescalescopehorizontal(pre, pointnum):
    # todo make these nested to loadscope?

    prestr = ''.join([chr(i) for i in pre[0].astype(np.uint8)])

    val = np.fromstring(prestr, sep=';')[:3]

    timerow = val[0] + val[1] * (np.arange(pointnum) - val[2])

    return timerow


def _rescalescopevertical(scope, pre):

    for idx, channel in enumerate(scope):
        prestr = ''.join([chr(i) for i in pre[idx].astype(np.uint8)])

        val = np.fromstring(prestr, sep=';')[3:6]

        scope[idx] = val[0] + val[1] * (channel - val[2])


def isindexedwave(wavename, filedata):
    # bool(0) is False
    # bool('anything else') is True

    # todo assumes data have a 'Note' dict key

    try:
        return bool(filedata['ExpInf']['IndexedWaves']) and \
           filedata['ExpInf']['IndexedWaves'] == wavename
    except KeyError:
        return False

    # return bool(NumberByKey(0, wavename, filedata['Note'], '=', '\n'))


def indexedwavevalue(filedata):

    # print(NumberByKey(0, 'IndexedValues', filedata['Note'], '=', ';\n'))

    # print(filedata['ExpInf'])

    return float(filedata['ExpInf']['IndexedValues'])

    # return NumberByKey(0, wavename, filedata['Note'], '=', '\n')


def imagegrid(x0, x1, img):

    yarray = np.linspace(x0[0], x1[0], img.shape[0])
    xarray = np.linspace(x0[1], x1[1], img.shape[1])

    return np.meshgrid(xarray, yarray)


def valueinnote(name, note):

    return NumberByKey(-1, name, note, '=', '\n')
