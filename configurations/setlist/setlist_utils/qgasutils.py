# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 15:51:35 2013

qgasfileio: Basic Quantum Gas Utilities functions


@author: ispielma
"""

import numpy

def ImageSlice(xVals, yVals, Image, Width, r0, Scaled = False):
    """
    Produces a pair of slices from image of a band with width Width centered at x0 = [x y]
    
    Scaled : False use pixels, and true compute scaling from (xvals and yvals) assuming
        they are linearly spaced    
    
    Currently Width and x,y are in scaled units, not pixel units
    the return will be ((xvals xslice) (yvals yslice)), where each entry is a numpy array.
    these are copies, not views.
    """

    if (Scaled):
        
        (xMin, yMin) = numpy.floor(GetPixelCoordsFromImage(r0, - Width/2, xVals, yVals));
        (xMax, yMax) = numpy.ceil(GetPixelCoordsFromImage(r0,  Width/2, xVals, yVals));
    else:
        (xMin, yMin) = r0 - numpy.round(Width/2);
        (xMax, yMax) = r0 + numpy.round(Width/2);
 
    # Extract bands of desired width
    # These are slices, so views of the initial data

    # Compute averages
    ySlice = Image[:,xMin:xMax].mean(1); # along y, so use x center
    xSlice = Image[yMin:yMax,:].mean(0); # along x, so use y center
    
    yValsSlice = yVals[:,0].copy();
    xValsSlice = xVals[0,:].copy();

    return ((xValsSlice, xSlice), (yValsSlice, ySlice));

def ImageCrop(xVals, yVals, Image, r0, Width, Scaled = False, Center = True):
    """
    crops an image along with the associated matrix of x and y 
    to a specified area and returns the cropped image
    this will be a copy not a view
    
    Image, xVals, yVals : (2D image, xvals, yvals) 
    r0 : ceter of ROI in physical units (two element list or array)
    Width : length of box-sides in physical units (two element list or array)

    Scaled : If true, will attempt to use the x and y waves, to generate pixel values
    
    Center: recenter on cropped region

    """   
    
    error = False;    
    
    if (Scaled):
        if(ScaleTest(xVals, yVals)):
            rMinPixel = numpy.floor(GetPixelCoordsFromImage(r0, -Width/2, xVals, yVals));
            rMaxPixel = numpy.ceil(GetPixelCoordsFromImage(r0, Width/2, xVals, yVals));
        else:
            rMinPixel = numpy.floor(r0)-numpy.floor(Width/2);
            rMaxPixel = numpy.ceil(r0)+numpy.ceil(Width/2);
            error = True;
    else:
        rMinPixel = numpy.floor(r0)-numpy.floor(Width/2);
        rMaxPixel = numpy.ceil(r0)+numpy.ceil(Width/2);
    
    Cropped_Image = Image[rMinPixel[1]:rMaxPixel[1],rMinPixel[0]:rMaxPixel[0]].copy();
    Cropped_xVals = xVals[rMinPixel[1]:rMaxPixel[1],rMinPixel[0]:rMaxPixel[0]].copy();
    Cropped_yVals = yVals[rMinPixel[1]:rMaxPixel[1],rMinPixel[0]:rMaxPixel[0]].copy();
    
    if (Center):
        Cropped_xVals -= r0[0];
        Cropped_yVals -= r0[1];
    
    return (Cropped_xVals, Cropped_yVals, Cropped_Image, error);
    
def GetPixelCoordsFromImage(r0, Offset, xVals, yVals):
    """
    Returns the pixel coordinates associated with the scaled values in the 2D arrays xVals and yVals
    remember in r0 the ordering is r0 = (x0, y0)
    """ 
    
    # Assume that the correct arrays were passed
    dy = yVals[1][0] - yVals[0][0];        
    dx = xVals[0][1] - xVals[0][0];
    
    y0 = yVals[0][0];
    x0 = xVals[0][0]; 
        
    #want offset to be an integer number of pixels
    Offset = numpy.round(Offset/numpy.array([dx,dy]));         
  
    return (r0 - numpy.array([x0, y0])) /numpy.array([dx, dy])+Offset;
    
def ScaleTest(xVals, yVals):
    """
    Returns the pixel coordinates associated with the scaled values in the 2D arrays xVals and yVals
    remember in r0 the ordering is r0 = (x0, y0)
    """ 
    
    # Assume that the correct arrays were passed
    dy = yVals[1][0] - yVals[0][0];        
    dx = xVals[0][1] - xVals[0][0];
    
    if ((dx == 0) or (dy == 0)):
        print("ImageSlice: generating scaled axes failed");
        print(dx,dy,xVals[0][1],xVals[0][0],yVals[1][0],yVals[0][0],xVals,yVals)
        return False;
    else:
        return True;
          
