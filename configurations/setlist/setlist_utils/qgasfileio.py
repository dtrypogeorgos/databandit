# -*- coding: utf-8 -*-
"""
Created on Thu Sep  5 12:45:39 2013

qgasfileio: Quantum Gas File IO procedures

@author: ispielma
"""
import importlib
from . import igorbin
import numpy
# importlib.reload(igorbin);
# reload(igorbin); # had to make this change for it to work

ISatCounts = 10000;

#==============================================================================
#
# The basic method of file loading will be to:
#     (1) load a file into a new dictionary.
#     (2) Register the basic varaibles
#         * Fill basic variables (including all provided in the variabes string)
#     (3) decide on the basic processing module
#         * register the variables to be generaited by the processing
#         * perform processingand fill
# Thus each loaded-image instance will contain only variables
# and those will have been generaitrd by that file
#
# Unlike the igor code, this implies that each loaded-file will only
# contain the required variables and there will never be extra's.
#
# At the higher level, a sequence will then be a dictionary of images
# from which we can generate arrays of variables.
#
#==============================================================================


def LoadImg( FileName ):
    """LOAD_IMG This loads an IGOR binary file saved by labview "
    Loads an image from igor and extracts a bunch of interesting "
    information from the image header"""

    IBWData = igorbin.LoadIBW(FileName);

    # I am going to store the experimental information in a dictionary
    Image = {"Note": IBWData["Note"]};

    # Add error checking here

    # Internal properties associlated with igor waves
    ExpInf = {"FileName": FileName};
#    Image.Experimental_Info.dx = A.dx;
#    Image.Experimental_Info.x0 = A.x0;
#    Image.Experimental_Info.x1 = A.x1;


    # Begin acquiting experimental information from note
    # I should consider passing some sort of default wave above.
    ExpInf["Camera"] = igorbin.StringByKey('', 'Camera_ID', Image["Note"],'=','\r\n');
    ExpInf["Experiment"] = igorbin.StringByKey('', 'Experiment', Image["Note"],'=','\r\n');
    ExpInf["DataType"] = igorbin.StringByKey('', 'DataType', Image["Note"],'=','\r\n');
    ExpInf["ImageType"] = igorbin.StringByKey('', 'ImageType', Image["Note"],'=','\r\n');
    ExpInf["magnification"] = igorbin.NumberByKey(1,'Magnification', Image["Note"],'=','\r\n');
    ExpInf["ImageDirection"] = igorbin.StringByKey('', 'ImageDirection', Image["Note"],'=','\r\n');

    # Indexed waves and variales contain all the information passed by the sequencer
    ExpInf["IndexedWaves"] = igorbin.StringByKey('', 'IndexedWaves', Image["Note"],'=','\r\n');
    ExpInf["IndexedValues"] = igorbin.StringByKey('', 'IndexedValues', Image["Note"],'=','\r\n');
    ExpInf["IndexedWaves"] = ExpInf["IndexedWaves"].strip(';\n\r');#cleaning up extra characters
    ExpInf["IndexedValues"] = ExpInf["IndexedValues"].strip(';\n\r');#cleaning up extra characters

    # Igor runs a function called SetCamDir here, but it seemed to just
    # turn "ImageDirection" into a string.

    if (ExpInf["Experiment"] == 'Rubidium_II'):
        ExpInf["Ibias"] = igorbin.NumberByKey(0,'I_Bias', Image["Note"],'=','\r\n');
        ExpInf["expand_time"] = igorbin.NumberByKey(1,'t_Expand', Image["Note"],'=','\r\n');
        ExpInf["detuning"] = igorbin.NumberByKey(0,'Detuning',Image["Note"],'=','\r\n');
    elif (ExpInf["Experiment"] == 'Rubidium_I'):
        ExpInf["Irace"] = igorbin.NumberByKey(0,'I_Race', Image["Note"],'=','\r\n');
        ExpInf["Ipinch"] = igorbin.NumberByKey(0,'I_Pinch', Image["Note"],'=','\r\n');
        ExpInf["trapmin0"] = igorbin.NumberByKey(0,'Trap_Min', Image["Note"],'=','\r\n');
        ExpInf["Ibias"] = igorbin.NumberByKey(0,'I_Bias', Image["Note"],'=','\r\n');
        ExpInf["expand_time"] = igorbin.NumberByKey(1,'t_Expand', Image["Note"],'=','\r\n');
        ExpInf["detuning"] = igorbin.NumberByKey(0,'Detuning', Image["Note"],'=','\r\n');
    elif (ExpInf["Experiment"] == 'Rubidium_Chip'):
        ExpInf["expand_time"] = igorbin.NumberByKey(1,'t_Expand', Image["Note"],'=','\r\n');
        ExpInf["detuning"] = igorbin.NumberByKey(0,'Detuning', Image["Note"],'=','\r\n');
    else:
        ExpInf["expand_time"] = igorbin.NumberByKey(1,'t_Expand', Image["Note"],'=','\r\n');
        ExpInf["detuning"] = igorbin.NumberByKey(0,'Detuning', Image["Note"],'=','\r\n');

    # There is a bunch of stuff in the Igor code here dealing with indexed

    # Now that the data is out of the file, we can begin to work with it.
    # First break the image into it's composite piece



    if (ExpInf["ImageType"] == "Raw"): # In this case the loaded file contains three images which need to be split apart and processed.
        # First split into two/three/four images and save as a list of raw data

        ImageDimensions = IBWData["Data"].shape;
        Raw = [];
        if (ExpInf["Camera"] == "PIXIS"):
            xsize = ImageDimensions[1] // 4;
            Raw.append( IBWData["Data"][:,0:xsize].astype(float) );
            Raw.append( IBWData["Data"][:,xsize:2*xsize].astype(float) );
            Raw.append( IBWData["Data"][:,2*xsize:3*xsize].astype(float) );
            Raw.append( IBWData["Data"][:,3*xsize:4*xsize].astype(float) );

        elif (ExpInf["Camera"] == "PIXISSlowReadOut"): # In this case the loaded file contains 1 frame with 2 images which need to be split apart and processed.
            xsize = ImageDimensions[1] // 2;

            Raw.append( IBWData["Data"][:,0:xsize].astype(float) );
            Raw.append( IBWData["Data"][:,xsize:2*xsize].astype(float) );

        elif (ExpInf['Camera'] == 'Flea3_uwaves'): # multiple images Flea3
            xsize = ImageDimensions[1] // 4
            Raw.append( IBWData["Data"][:,0:xsize].astype(float) );
            Raw.append( IBWData["Data"][:,xsize:2*xsize].astype(float) );
            Raw.append( IBWData["Data"][:,2*xsize:3*xsize].astype(float) );
            Raw.append( IBWData["Data"][:,3*xsize:4*xsize].astype(float) );

        else:
            xsize = ImageDimensions[1] // 3;
            Raw.append( IBWData["Data"][:,0:xsize].astype(float) );
            Raw.append( IBWData["Data"][:,xsize:2*xsize].astype(float) );
            Raw.append( IBWData["Data"][:,2*xsize:3*xsize].astype(float) );


        #
        # Now store the base images
        # to be considered immutable
        #

#        Image["Raw"] = IBWData['Data'] # return data only
        Image['Raw'] = Raw

        # Create the correct data
        if (ExpInf["DataType"] == "Absorption" or ExpInf["DataType"] == "Absorbsion"): # Account for historical spelling error...
            # Note use of conditionl to add one to places with zero conts in denominator
            Image["OptDepth"] = -numpy.log(( (Image["Raw"][0] < 1) + Image["Raw"][0])/( (Image["Raw"][1] < 1) + Image["Raw"][1]));
        elif (ExpInf["DataType"] == "Fluorescence"):
            Image["OptDepth"] = Image["Raw"][0] - Image["Raw"][1];
        elif (ExpInf["DataType"] == "PhaseContrast"):
            Image["OptDepth"] = Image["Raw"][0]/Image["Raw"][1];
        elif (ExpInf["DataType"] == "RawImage"):
            Image["OptDepth"] = Image["Raw"][0].copy();
        else:
            # Note use of conditionl to add one to places with zero conts in denominator
            Image["OptDepth"] = Image["Raw"][0]/((Image["Raw"][1] < 1) * numpy.abs(Image["Raw"][1] + 1) + Image["Raw"][1]);

    else:	# Data is pre-processed (difference for fluresence, ratio for absorption)
        Image["OptDepth"] = IBWData["Data"].astype(float);

    Image["ExpInf"] = ExpInf;

    # Update based on properties of the camera
    # This should be updated AFTER the image is broken into Raw1 to Raw4

    Update_Magnification(Image);

    # Extract any optional variables associated with this wave
    ExpandIndexedString( Image );

    # Generate arrays of the x and y values
    yarray = numpy.linspace(Image["ExpInf"]["x0"][0],Image["ExpInf"]["x1"][0],Image["OptDepth"].shape[0]);
    xarray = numpy.linspace(Image["ExpInf"]["x0"][1],Image["ExpInf"]["x1"][1],Image["OptDepth"].shape[1]);
    (xVals,yVals) = numpy.meshgrid(xarray,yarray);

    Image["xVals"] = xVals;
    Image["yVals"] = yVals;

    Image['Raw'] = IBWData["Data"]

    # ComputeTrapProperties(); # Doesn't belong in core image loading function
    return Image


def FileNameTuple(FileNameTuple, InitialIndexTuple, FinalIndexTuple, Extension = ""):
    """
    FileNameTuple : this function returns a tuple of files that should be evaulated
        Takes as parameters a tuple of file name headers,
        starting indices and ending indices
    """

    # Verify that all tuples have non-zero length
    if (len(FileNameTuple) == 0 or len(InitialIndexTuple) == 0 or len(FinalIndexTuple) == 0):
        print("FileNameTuple: passed initial object of zero length");
        return ();

    # Verify that both index tuples have the same length
    if (len(InitialIndexTuple) != len(FinalIndexTuple)):
        print("FileNameTuple: index tuples have different length");
        return ();

    # Make the NameLen tuple the same length as the others by padding
    # with it's last element
    NameLen = len(FileNameTuple);
    IndexLen =  len(InitialIndexTuple);

    if (NameLen < IndexLen):
        FileNameTupleLong = FileNameTuple + (FileNameTuple[-1],)*(IndexLen-NameLen);
    else:
        FileNameTupleLong = FileNameTuple;

    # Now build the tuple of file names
    FileList = [];
    for FileNameHeader, InitialIndex, FinalIndex in zip(FileNameTupleLong, InitialIndexTuple, FinalIndexTuple):

        Initial = int(InitialIndex);
        Final = int(FinalIndex);

        if (Final < Initial):
            print("FileNameTuple: Initial index must be smaller than Final index");
            return ();

        # Since we want the last element, add one
        FileList += [FileNameHeader +  "{:0>4d}".format(i) + Extension for i in range(Initial, Final+1)];

    return tuple(FileList);


#==============================================================================
#
# The below functions are helper functions intended for use only within
# the qgasfileio module
#
#==============================================================================

def Update_Magnification( Image ):
# UPDATE_MAGNIFICATION Uses the camera settings in the loaded image
# No further information

    if (Image["ExpInf"]["Camera"] == 'PixelFly'): # Pixelfly Camera
        delta_X = 6.45 / Image["ExpInf"]["magnification"];
        delta_Y = 6.45 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'PI'):  # Old PI Camera
        delta_X = 15 / Image["ExpInf"]["magnification"];
        delta_Y = 15 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'LG3'):  # LG3 Frame grabber
        delta_X = 10 / Image["ExpInf"]["magnification"];
        delta_Y = 10 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'THOR'):  # Thorlabs CCD Cameras
        delta_X = 7.4 / Image["ExpInf"]["magnification"];
        delta_Y = 7.4 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'Flea3'):  # PointGrey Flea3
        delta_X = 5.6 / Image["ExpInf"]["magnification"];
        delta_Y = 5.6 / Image["ExpInf"]["magnification"];
        #delta_X = 5.6 / 6.25;
        #delta_Y = 5.6 / 6.25;
    elif (Image["ExpInf"]["Camera"] == 'PIXIS'):  # PI Pixis camera
        delta_X = 13 / Image["ExpInf"]["magnification"];
        delta_Y = 13 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'PIXISSlowReadOut'):  # PI Pixis camera, slow readout
        delta_X = 13 / Image["ExpInf"]["magnification"];
        delta_Y = 13 / Image["ExpInf"]["magnification"];
    elif (Image["ExpInf"]["Camera"] == 'BlackFly'):  # BlackFly GigE
        delta_X = 3.45 / Image["ExpInf"]["magnification"]
        delta_Y = 3.45 / Image["ExpInf"]["magnification"]
    else :
        delta_X = 1 / Image["ExpInf"]["magnification"];
        delta_Y = 1 / Image["ExpInf"]["magnification"];

    Image["ExpInf"]["dx"] = numpy.array([delta_X, delta_Y]);
    Image["ExpInf"]["x0"] = -numpy.array(Image["OptDepth"].shape) * Image["ExpInf"]["dx"] / 2;
    Image["ExpInf"]["x1"] = +numpy.array(Image["OptDepth"].shape) * Image["ExpInf"]["dx"] / 2;

def ExpandIndexedString( Image ):
    """ExpandIndexedString Creates new variables in Image["ExpInf"] for each indexed
    wave - IndexedValue pair """

    # Indexed waves and variales contain all the information passed by the sequencer
    WaveNameList = Image["ExpInf"]["IndexedWaves"].split(";");
    WaveValuesList = Image["ExpInf"]["IndexedValues"].split(";");

    # Strip empty strings from both lists
    WaveNameListStrip = [x for x in WaveNameList if x != ""];
    WaveValuesListStrip = [float(x) for x in WaveValuesList if x != ""];

    # Verify that both lists have the same length
    if (len(WaveNameListStrip) == len(WaveValuesListStrip)):
        NewDict = {k : v for k,v in zip(WaveNameListStrip, WaveValuesListStrip)}
    else:
        NewDict = {};

    Image["ExpInf"].update(NewDict);
