from datetime import datetime
import os
from setlist_utils import qgasfileio as qio
from setlist_utils import loadfile

# if databandit not in $PYTHONPATH
# import sys
# sys.path.append('/Users/dimitris/Desktop/databandit')
from databandit import Config


class SetList(Config):
    def __init__(self, **kwargs):
        # TODO documentation

        self.date = datetime.strptime(kwargs['date'], '%Y%m%d')
        datapath_parts = self.date.strftime('Data %Y %B %d/').split()
        self.folderpath = os.path.join(os.getcwd(), *datapath_parts)
        self.folderpath = os.path.relpath(self.folderpath)

        # cwd = Path.cwd()
        # self.folderpath = cwd.joinpath(*datapath_parts)
        #
        # print(self.folderpath)

        self.devices = kwargs['devices']
        self.filerange = kwargs['filerange']

        try:
            self.prefix = kwargs['prefix']
        except KeyError:
            self.prefix = 'Data'

        # print(self.date, self.folderpath)
        # self._devices = None
        # self._filerange = None
        # self._folderpath = None

    def filenames(self):
        files = ()
        for device in self.devices:
            files += qio.FileNameTuple((self.folderpath + '/' + device +
                                        self.date.strftime('_%d%b%Y_'),),
                                       (self.filerange[0],),
                                       (self.filerange[1] - 1,), '.ibw')

        # Return files in a list. This puts different devices that logically
        # correspond to the same shot on different rows in the dataframe.
        # You can make it MultiIndex at postprocess if you want.
        return files

    def outputfile(self):
        return self.prefix + self.date.strftime('_%d%b%Y_') + \
                  '{:04d}_{:04d}'.format(*self.filerange) + '.h5'

    def loadfile(self, filename):
        return loadfile(filename)
